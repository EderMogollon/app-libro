package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_fable3.*
import kotlinx.android.synthetic.main.activity_story3.*

class FableActivity3 : AppCompatActivity() {
    var nameText: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fable3)
        buttonView = findViewById(R.id.button_view_fable3)
        generateFable()
    }

    private fun generateFable() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        text_fable3.text = "– ¡Muchas gracias, ratón $nameText! – sonrió el león agradecido – . " +
                " Me has salvado la vida. Ahora entiendo que nadie es menos que nadie " +
                "y que cuando uno se porta bien con los demás, tiene su recompensa. " +

                "Se fundieron en un abrazo y a partir de entonces, el león dejó que el ratoncillo $nameText " +
                " trepara sobre su lomo siempre que quisiera.”."

        buttonView.setOnClickListener{
            var bundle2 = Bundle()
            bundle2.apply{ putString("key_name",nameText) }
            var intent = Intent(this,FableActivity4::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}