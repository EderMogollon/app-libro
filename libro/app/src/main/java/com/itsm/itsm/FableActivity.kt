package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_fable.*
import kotlinx.android.synthetic.main.activity_story.*
import kotlinx.android.synthetic.main.activity_story.text_story

class FableActivity : AppCompatActivity() {
    var nameText: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fable)
        buttonView = findViewById(R.id.button_view_fable)
        generateFable()
    }

    private fun generateFable() {

        val bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",nameText) }

        text_fable.text = "En la sabana del continente Africano, era gobernado por un enorme y feroz leon que era el Rey de todos los animales" +
                "que ahi vivian, todos vivian en paz gracias al leno que lo gobernaba.  " +
                "Un dia Dormía el león cuando un ratón llamado $nameText empezó a juguetear encima de su cuerpo. Despertó el león y a atrapó a $nameText." +
                "A punto de ser devorado $nameText, el ratón le pidió que le perdonara, prometiéndole pagarle en el futuro "
        buttonView.setOnClickListener{
            intent = Intent(this,FableActivity2::class.java).apply { putExtras(bundle2) }
            startActivity(intent)
        }

    }
}