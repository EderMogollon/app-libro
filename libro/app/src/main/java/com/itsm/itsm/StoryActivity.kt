package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_story.*

class StoryActivity : AppCompatActivity() {
    var nameText: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story)
        buttonView = findViewById(R.id.button_view)
        generateStory()
    }

    private fun generateStory() {

        val bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }
        text_story.text = "La brisa marina acariciaba los dedos de $nameText, mientras pasaba otra página de aquella absorbente novela " +
                "Una ligera brisa marina nocturna hizo que el vello del lector se erizase, las mareas se arrastraban, " +
                " igual que sus dedos intentando retener en su cuerpo  "

        buttonView.setOnClickListener{
            var bundle2 = Bundle()
            bundle2.apply { putString("key_name",nameText) }

            intent = Intent(this,StoryActivity2::class.java).apply { putExtras(bundle2) }
            startActivity(intent)
        }

    }
}