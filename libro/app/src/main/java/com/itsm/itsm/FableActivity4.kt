package com.itsm.itsm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fable4.*

class FableActivity4 : AppCompatActivity() {
    var nameText: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fable4)
        generateFable()
    }

    private fun generateFable() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        text_fable4.text = "Moraleja: $nameText nunca hagas de menos a nadie porque parezca más débil o menos inteligente que tú." +
                "Sé bueno con todo el mundo y los demás serán buenos contigo."
    }
}