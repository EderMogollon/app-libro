package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_story2.*

class StoryActivity2 : AppCompatActivity() {
    lateinit var buttonView: Button
    var name_text: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story2)
        buttonView = findViewById(R.id.button_view2)
        generateStory()
    }

    private fun generateStory() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            name_text = it.getString("key_name")
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_text) }

        text_story2.text = "las reacciones que le provocaba la sensualidad de Lady Charis. Embelesado por las tintas de $name_text,  " +
                "a suave temperatura marítima en otoño y el creer que podía oler a su ficticia princesa, empezó a experimentar en carne propia los pequeños  " +
                "mordiscos en el cuello, las uñas arañando sus carnes y la suave nariz de la dama más allá de las líneas.   " +
                "Convencido por las sensaciones evocadas por el autor, cerró los ojos y dejó que su mente volase libremente. "
        buttonView.setOnClickListener{
            var intent = Intent(this,StoryActivity3::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}