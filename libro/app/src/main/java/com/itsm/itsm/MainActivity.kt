package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var buttonGenerate: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonGenerate = findViewById(R.id.button_generate)

        create()
    }

    private fun create() {
        buttonGenerate.setOnClickListener{
            var editText: EditText = findViewById(R.id.name)
            var nameText = editText.text.toString()

            if(nameText.isEmpty()){
                Toast.makeText(this, "Debe ingresar un nombre", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            var bundle = Bundle()
            bundle.apply {
                putString("key_name",nameText)
            }

            val intent = Intent(this,Option::class.java).apply{
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}