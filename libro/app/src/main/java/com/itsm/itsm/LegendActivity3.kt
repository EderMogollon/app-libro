package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_fable3.*
import kotlinx.android.synthetic.main.activity_legend3.*
import kotlinx.android.synthetic.main.activity_story3.*

class LegendActivity3 : AppCompatActivity() {
    var nameText: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_legend3)

        generateLegend()
    }

    private fun generateLegend() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        text_legend3.text = " Esa noche lo encontraron intentando morder a un lugareño,  " +
                " así que le clavaron una estaca de madera y luego sepultaron su cuerpo bajo una pila de ladrillos.  " +
                "Años después un árbol creció de entre los ladrillos a partir de la estaca de madera,  " +
                "y se dice que al cortar sus ramas aparecen dentro del corte regueros de sangre,  " +
                " de las víctimas del vampiro de Guadalajara. ”"


        }
    }
