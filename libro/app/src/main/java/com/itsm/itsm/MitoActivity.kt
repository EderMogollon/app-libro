package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_mito.*
import kotlinx.android.synthetic.main.activity_story.*
import kotlinx.android.synthetic.main.activity_story.text_story

class MitoActivity : AppCompatActivity() {
    var nameText: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mito)
        buttonView = findViewById(R.id.button_view_mito)
        generateMito()
    }

    private fun generateMito() {

        val bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",nameText) }

        text_mito.text = "El dios Quetzalcóatl bajó un día a dar un paseo largo por la tierra,   " +
                "al caer la noche se encontraba cansado y hambriento sin saber qué comer cuando  " +
                " pasó enfrente de él un conejo que le ofreció de su comida, a lo que el dios se negó, pues no era algo que acostumbrara comer. "

        buttonView.setOnClickListener{
            intent = Intent(this,MitoActivity2::class.java).apply { putExtras(bundle2) }
            startActivity(intent)
        }

    }
}