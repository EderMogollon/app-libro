package com.itsm.itsm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_story4.*

class StoryActivity4 : AppCompatActivity() {
    var name_text: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story4)
        generateStory()
    }

    private fun generateStory() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            name_text = it.getString("key_name")
        }

        text_story4.text = "ahí por donde pasaba había otro camino que era el del camposanto. Al momento, $name_text "+
                            "Sus ojos se abrieron dentro de las negras mareas, el rostro apolíneo de Lady Charis lo miraba, " +
                            "su corazón se derritió de dulzura por un par de segundos, antes de que las fauces se abrieran " +
                "y un grito quedase ahogado por cinco metros de agua."+
                "Horas más tarde, el libro apareció en la arena mojado, rasgado y marcado por unos labios ensangrentados."

    }
}