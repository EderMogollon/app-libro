package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_story3.*

class StoryActivity3 : AppCompatActivity() {
    var name_text: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story3)
        buttonView = findViewById(R.id.button_view3)
        generateStory()
    }

    private fun generateStory() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            name_text = it.getString("key_name")
        }

        text_story3.text = "Una astuta sirena, quien no precisó de técnicas de seducción físicas, tomó la obra en sus manos para continuar leyendo con voz melosa, mientras una compañera emulaba las tintas,   " +
                "uando una tercera lo arrastraba lentamente. El solitario varón, quien se sentía en un sueño disfrutando de lo que consideraba el arte de la inmersión gracias a la pluma del autor, " +
                " $name_text fue callado con un beso poco antes de caer al mar. "



        buttonView.setOnClickListener{
            var bundle2 = Bundle()
            bundle2.apply{ putString("key_name",name_text) }
            var intent = Intent(this,StoryActivity4::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}