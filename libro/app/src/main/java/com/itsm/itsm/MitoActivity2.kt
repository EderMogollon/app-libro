package com.itsm.itsm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_mito2.*

class MitoActivity2 : AppCompatActivity() {
    var nameText: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mito2)
        generateMito()
    }

    private fun generateMito() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        text_mito2.text = "El conejo se ofreció en sacrificio para ser comido y Quetzalcóatl en agradecimiento le prometió que de entonces en adelante sería recordado. " +
                "Lo tomó y lo elevó hasta la luna para estampar ahí su figura diciéndole “En homenaje a tu nobleza para que seas recordado por todos”."
    }
}