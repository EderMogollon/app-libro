package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_fable2.*
import kotlinx.android.synthetic.main.activity_legend2.*

class LegendActivity2 : AppCompatActivity() {
    lateinit var buttonView: Button
    var nameText: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_legend2)
        buttonView = findViewById(R.id.button_view_legend2)
        generateLegend()
    }

    private fun generateLegend() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",nameText) }

        text_legend2.text = "De hecho, desde la llegada de $nameText, era un hombre misterioso   " +
                    "empezaron a aparecer primero cadáveres de animales, y luego cuerpos sin vida de niños, todos ellos desangrados.  "+
                    "Una noche, las gentes del poblado decidieron buscar al extranjero para enfrentarlo,  "+
                    "asumiendo que él era el autor de los hechos.  "


        buttonView.setOnClickListener{
            var intent = Intent(this,LegendActivity3::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}