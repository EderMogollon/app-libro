package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_fable.*
import kotlinx.android.synthetic.main.activity_legend.*
import kotlinx.android.synthetic.main.activity_story.*
import kotlinx.android.synthetic.main.activity_story.text_story

class LegendActivity : AppCompatActivity() {
    var nameText: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_legend)
        buttonView = findViewById(R.id.button_view_legend)
        generateLegend()
    }

    private fun generateLegend() {

        val bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }



        text_legend.text = "Hace muchos años, un extranjero llamado $nameText proveniente de Europa llegó a un poblado    " +
                "de la zona de Guadalajara, México.  " +
                "  $nameText Era una persona extraña y reservada, pero su falta de interés en socializar   " +
                "con la gente de la región no era lo más inquietante.   "

        buttonView.setOnClickListener{
            var bundle2 = Bundle()
            bundle2.apply{ putString("key_name",nameText) }

            intent = Intent(this,LegendActivity2::class.java).apply { putExtras(bundle2) }
            startActivity(intent)
        }

    }
}