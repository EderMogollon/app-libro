package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_fable2.*

class FableActivity2 : AppCompatActivity() {
    lateinit var buttonView: Button
    var nameText: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fable2)
        buttonView = findViewById(R.id.button_view_fable2)
        generateFable()
    }

    private fun generateFable() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",nameText) }

        text_fable2.text = "El león echó a reír y lo dejó marchar. Días después, unos cazadores apresaron al rey de la selva y lo ataron con una cuerda." +
                    "Al oír el ratón $nameText los lamentos del león, corrió al lugar y royó la cuerda,"+
                    "dejándolo libre. Días atrás $nameText  - le dijo al leon -,"+
                    "te burlaste de mí pensando que nada podría hacer por ti en agradecimiento."+
                "Ahora es bueno que sepas que los pequeños ratones somos agradecidos y cumplidos"

        buttonView.setOnClickListener{
            var intent = Intent(this,FableActivity3::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}